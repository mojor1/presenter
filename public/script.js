// Функция регистрации
function register() {
  var usernameInput = document.getElementById('reg-username');
  var passwordInput = document.getElementById('reg-password');

  var username = usernameInput.value;
  var password = passwordInput.value;

  if (username && password) {
    // Проверяем, есть ли уже пользователь с таким именем
    if (!localStorage.getItem(username)) {
      // Сохраняем данные пользователя в Local Storage
      localStorage.setItem(username, password);
      alert('Регистрация успешна!');
      // Очищаем поля ввода
      usernameInput.value = '';
      passwordInput.value = '';
    } else {
      alert('Пользователь с таким именем уже существует!');
    }
  } else {
    alert('Пожалуйста, заполните все поля!');
  }
}

// Функция авторизации
function login() {
  var usernameInput = document.getElementById('login-username');
  var passwordInput = document.getElementById('login-password');

  var username = usernameInput.value;
  var password = passwordInput.value;

  if (username && password) {
    // Проверяем, соответствуют ли введенные данные данным пользователя в Local Storage
    var storedPassword = localStorage.getItem(username);
    if (storedPassword === password) {
      alert('Авторизация успешна!');
      // Очищаем поля ввода
      usernameInput.value = '';
      passwordInput.value = '';
      window.location.href = 'giftpage.html'
    } else {
      alert('Неверное имя пользователя или пароль!');
    }
  } else {
    alert('Пожалуйста, заполните все поля!');
  }
}

// Функция переключения между вкладками
function switchTab(tabId) {
  var registerTab = document.getElementById('register');
  var loginTab = document.getElementById('login');

  if (tabId === 'register') {
    registerTab.style.display = 'block';
    loginTab.style.display = 'none';
  } else if (tabId === 'login') {
    registerTab.style.display = 'none';
    loginTab.style.display = 'block';
  }
}